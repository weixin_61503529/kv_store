#include "kvstore.h"
#include "reactor.h"
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){
    if(argc!= 3){
        cout << "Usage: " << argv[0] << " <ip> <port>" << endl;
        return 1;
    }
    work* mywork = new KVStore();
    Reactor myreactor(argv[1],argv[2],mywork);
    myreactor.Reactor_init();
    myreactor.Run();
    return 0;
}