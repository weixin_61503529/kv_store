#include "kvstore.h"



// 红黑树
string rbtree_store::kvs_get(string key) {
    if(m_data->find(key) == m_data->end()) {
        return "";
    }

    return (*m_data)[key];
}
bool rbtree_store::kvs_set(string key,string value) {
    (*m_data)[key] = value;
    return true;
}
bool rbtree_store::kvs_modify(string key,string value) {
    if(m_data->find(key) == m_data->end()){
        return false;
    }
    (*m_data)[key] = value;
    return true;
}
bool rbtree_store::kvs_del(string key) {
    if(m_data->find(key) == m_data->end()) {
        return false;
    }
    m_data->erase(key);
    return true;
}
string rbtree_store::kvs_get_all() {
    stringstream ss;
    for(auto it = m_data->begin(); it != m_data->end(); it++) {
        ss << it->first << " : " << it->second << endl;
    }
    return ss.str();
}
bool rbtree_store::kvs_exist(string key) {
    if(m_data->find(key) == m_data->end()) {
        return false;
    }
    
    return true;
}

// 哈希表
string hash_store::kvs_get(string key) {
    if(m_data->find(key) == m_data->end()) {
        return "";
    }

    return (*m_data)[key];
}
bool hash_store::kvs_set(string key,string value) {
    (*m_data)[key] = value;
    return true;
}
bool hash_store::kvs_modify(string key,string value) {
    if(m_data->find(key) == m_data->end()){
        return false;
    }
    (*m_data)[key] = value;
    return true;
}
bool hash_store::kvs_del(string key) {
    if(m_data->find(key) == m_data->end()) {
        return false;
    }
    m_data->erase(key);
    return true;
}
string hash_store::kvs_get_all() {
    stringstream ss;
    for(auto it = m_data->begin(); it != m_data->end(); it++) {
        ss << it->first << " : " << it->second << endl;
    }
    return ss.str();
}
bool hash_store::kvs_exist(string key) {
    if(m_data->find(key) == m_data->end()) {
        return false;
    }
    
    return true;
}



// 业务函数
bool KVStore::recv_done(char* rbuffer){
    string rbuf(rbuffer);

    if(rbuf.empty()) return "";
    // 解析请求
    stringstream tokens(rbuf);
    vector<string> request;
    string token;
    while(tokens >> token){
        request.push_back(token);
    }

    // 处理请求
    if(request.at(0) == "get"){
        string result = m_store->kvs_get(request.at(1));

        if(result.empty()){
            m_answer = "NOT_EXIST";
        }
        else{
            m_answer = result;
        }
    }
    else if(request.at(0) == "set"){
        m_store->kvs_set(request.at(1),request.at(2));
        m_answer = "OK";
    }
    else if(request.at(0) == "mod"){
        bool result = m_store->kvs_modify(request.at(1),request.at(2));
        if(result){
            m_answer = "OK";
        }
        else{
            m_answer = "NOT_EXIST";
        }
    }
    else if(request.at(0) == "del"){
        bool result = m_store->kvs_del(request.at(1));
        if(result){
            m_answer = "OK";
        }
        else{
            m_answer = "NOT_EXIST";
        }
    }
    else if(request.at(0) == "get_all"){
        m_answer = m_store->kvs_get_all();
    }
    else if(request.at(0) == "exist"){
        bool result = m_store->kvs_exist(request.at(1));
        if(result){
            m_answer = "EXIST";
        }
        else{
            m_answer = "NOT_EXIST";
        }
    }
    else{
        m_answer = "ERROR";
    }

    //windows
    m_answer += "\r\n";
    return true;
}

bool KVStore::send_done(char* wbuffer, int& wlength){

    strcpy(wbuffer, m_answer.c_str());
    wbuffer[m_answer.size()] = '\0';
    wlength = m_answer.size() + 1;

    return true;
}