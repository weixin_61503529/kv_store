#ifndef __REACTOR_H__
#define __REACTOR_H__
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <poll.h>
#include <sys/epoll.h>
#include <errno.h>
#include <sys/time.h>
#include <iostream>
#include <string>
#include <arpa/inet.h>
#include <tuple>
#include <sstream>
using namespace std;

#define BUFFER_LENGTH		1024 //数据内存大小
#define CONNECTION_SIZE			1024 // 1024 * 1024 最大可监听的文件符
#define TIME_SUB_MS(tv1, tv2)  ((tv1.tv_sec - tv2.tv_sec) * 1000 + (tv1.tv_usec - tv2.tv_usec) / 1000)

// 目标：使用策略者模式，
// 稳定点：reactor的反应池处理过程，内部的API
// 变化点：业务


// 业务的模板
class work{
public:
    virtual bool recv_done(char* buffer) = 0;
    virtual bool send_done(char* wbuffer, int& wlength) = 0;

};

struct conn {
	int fd;

	char rbuffer[BUFFER_LENGTH];
	int rlength;

	char wbuffer[BUFFER_LENGTH];
	int wlength;

};

class Reactor{
public:
    Reactor(string ip, string port, work* work = nullptr): 
    _ip(ip), _port(stoi(port)), _work(work){
        _epfd = 0;
        struct conn conn_list[CONNECTION_SIZE] = {0};
    }
    ~Reactor(){
        close(_epfd);
        close(_fd);
        delete _work;
    }
    void Reactor_init();
    void Run();
public:
    struct conn conn_list[CONNECTION_SIZE];

private:
    int accept_cb();
    int recv_cb(int fd);
    int send_cb(int fd);
    void set_event(int fd, int event, int flag);

private:
    int _epfd;    // 事件堆文件符
    int _fd;      // 服务端文件符
    string _ip;
    unsigned short _port;

    work* _work;  // 策略者模式的业务对象

    struct timeval begin; // 测试时间
};




#endif