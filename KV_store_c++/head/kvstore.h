#ifndef __KV_STORE_H__
#define __KV_STORE_H__


#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include "reactor.h"
#include <sstream>
#include <functional>
#include <unordered_map>
#include <cstring>
using namespace std;

// 数据结构选择
#define KVS_RB_TREE		1
#define KVS_HASH_TABLE	2

// 网络类型选择
#define NETWORK_REACTOR 	1
#define NETWORK_NTYCO		0

#define NETWORK_SELECT		NETWORK_REACTOR

#define KVS_MAX_TOKENS		128  //最大支持的token数量



// 数据结构抽象类，用于统一接口
class ab_store{
public:
	virtual string kvs_get(string key) = 0;
	virtual bool kvs_set(string key,string value) = 0;
	virtual bool kvs_modify(string key,string value) = 0;
	virtual bool kvs_del(string key) 	= 0;
	virtual string kvs_get_all() = 0;
	virtual bool kvs_exist(string key) = 0;
	virtual ~ab_store(){}
};

class rbtree_store:public ab_store{
public:
	rbtree_store(){
		m_data = new map<string,string>();
	}
	~rbtree_store(){
		delete m_data;
		m_data = nullptr;
	}
public:	
	string kvs_get(string key) override;
	bool kvs_set(string key,string value) override;
	bool kvs_modify(string key,string value) override;
	bool kvs_del(string key) override;
	string kvs_get_all() override;
	bool kvs_exist(string key) override;
private:
	map<string,string>* m_data;
};

class hash_store:public ab_store{
public:
	hash_store(){
		m_data = new unordered_map<string,string>();
	}
	~hash_store(){
		delete m_data;
		m_data = nullptr;
	}
public:
	string kvs_get(string key) override;
	bool kvs_set(string key,string value) override;
	bool kvs_modify(string key,string value) override;
	bool kvs_del(string key) override;
	string kvs_get_all() override;
	bool kvs_exist(string key) override;
private:
	unordered_map<string,string>* m_data;
};




class KVStore:public work{
public:
	KVStore(int store_type = KVS_RB_TREE){
		if(store_type == KVS_RB_TREE){
			m_store = new rbtree_store();
		}
		else{
			m_store = new hash_store();
		}
	}
	~KVStore(){
		delete m_store;
		m_store = nullptr;
	}
public:
    bool recv_done(char* buffer) override;
    bool send_done(char* wbuffer, int& wlength) override;
private:
    ab_store* m_store;
	string m_answer;

};






#endif



